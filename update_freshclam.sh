sed -i '/HTTPProxyServer/d' /etc/clamav/freshclam.conf
sed -i '/HTTPProxyPort/d' /etc/clamav/freshclam.conf
sed -i '/HTTPProxyUsername/d' /etc/clamav/freshclam.conf
sed -i '/HTTPProxyPassword/d' /etc/clamav/freshclam.conf
sed -i '/ScriptedUpdates/d' /etc/clamav/freshclam.conf

if [ ! -z "$HTTP_PROXY_HOST" ]
then
  echo 'HTTPProxyServer '$HTTP_PROXY_HOST >> /etc/clamav/freshclam.conf
fi

if [ ! -z "$HTTP_PROXY_PORT" ]
then
  echo 'HTTPProxyPort '$HTTP_PROXY_PORT >> /etc/clamav/freshclam.conf
fi

if [ ! -z "$HTTP_PROXY_USER" ]
then
  echo 'HTTPProxyUsername '$HTTP_PROXY_USER >> /etc/clamav/freshclam.conf
fi

if [ ! -z "$HTTP_PROXY_PASSWORD" ]
then
  echo 'HTTPProxyPassword '$HTTP_PROXY_PASSWORD >> /etc/clamav/freshclam.conf
fi

echo 'ScriptedUpdates no' >> /etc/clamav/freshclam.conf

/usr/bin/freshclam --datadir="/usr/local/apache2/htdocs/" -u root

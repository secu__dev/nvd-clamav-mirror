# nvd-clamav-mirror

A NVD & ClamAV Docker mirror

## Environment variables required

- NVD Mirror
  
    JAVA_OPTS (Java proxy format, "-Dhttp.proxyHost=x.y.z.a -Dhttp.proxyPort=80 -Dhttps.proxyHost=x.y.z.a -Dhttps.proxyPort=80")

- ClamAV Mirror

    Custom proxy settings :
    
    HTTP_PROXY_HOST
    HTTP_PROXY_PORT
    HTTP_PROXY_USER
    HTTP_PROXY_PASSWORD
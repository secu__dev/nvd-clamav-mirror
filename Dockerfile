FROM alpine/git
WORKDIR /app
RUN git clone https://github.com/stevespringett/nist-data-mirror.git

FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY --from=0 /app/nist-data-mirror /app
RUN mvn clean package

FROM httpd:alpine
WORKDIR /app
COPY --from=1 /app/target/nist-data-mirror.jar /app
COPY update_freshclam.sh /app
RUN apk update && apk add --no-cache openjdk8-jre && apk add --no-cache clamav curl
RUN chmod a+x /app/update_freshclam.sh \
 && rm -f /usr/local/apache2/htdocs/*.html \
 && sed -i '/HTTPProxyServer/d' /etc/clamav/freshclam.conf \
 && sed -i '/HTTPProxyPort/d' /etc/clamav/freshclam.conf \
 && sed -i '/HTTPProxyUsername/d' /etc/clamav/freshclam.conf \
 && sed -i '/HTTPProxyPassword/d' /etc/clamav/freshclam.conf \
 && sed -i '/ScriptedUpdates/d' /etc/clamav/freshclam.conf \
 && echo '0  1  *  *  *    java $JAVA_OPTS -jar /app/nist-data-mirror.jar /usr/local/apache2/htdocs json' > /etc/crontabs/root \
 && echo '0  */2  *  *  *    /usr/bin/freshclam --datadir="/usr/local/apache2/htdocs/" -u root' >> /etc/crontabs/root \
 && echo '0  1  *  *  *    curl https://raw.githubusercontent.com/Retirejs/retire.js/master/repository/jsrepository.json -o /usr/local/apache2/htdocs/jsrepository.json' >> /etc/crontabs/root

# Running the recovery at launch, to not wait until 1am the next day to get data
CMD (java $JAVA_OPTS -jar /app/nist-data-mirror.jar /usr/local/apache2/htdocs json) \
 && ( crond -f -l 2 & ) \
 && (/app/update_freshclam.sh) \
 && (curl https://raw.githubusercontent.com/Retirejs/retire.js/master/repository/jsrepository.json -o /usr/local/apache2/htdocs/jsrepository.json) \
 && httpd -D FOREGROUND
